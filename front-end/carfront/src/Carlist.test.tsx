// import { QueryClient, QueryClientProvider } from 
//   '@tanstack/react-query';
// import { describe, test } from 'vitest';
// import { render, screen, waitFor, fireEvent } from '@testing-library/react';
// import '@testing-library/jest-dom/vitest';
// import Carlist from './components/Carlist';
// import userEvent from '@testing-library/user-event';


// const queryClient = new QueryClient({
//   defaultOptions: {
//     queries: {
//       retry: false,
//     },
//   },
// });
// const wrapper = ({
//   children } : { children: React.ReactNode }) => (
//     <QueryClientProvider client = {
//       queryClient}>{children}
//     </QueryClientProvider>);
// describe("Carlist tests", () => {
//     test("component renders", () => {
//       render(<Carlist />, { wrapper });
//       expect(screen.getByText(/Loading/i)
//         ).toBeInTheDocument();
//     })
//     // test("Cars are fetched", async () => {
//     //   render(<Carlist />, { wrapper });
//     //   await waitFor(() => screen.getByText(/New Car/i), {timeout: 10000});
//     //   expect(screen.getByText(/Ford/i)).toBeInTheDocument();
//     // })
//     test("Open new car modal", async () => {
//         render(<Carlist />, { wrapper });
//         const carButton = await screen.findByText(/NEW CAR/i)
//         await userEvent.click(carButton);
//         expect(screen.getByText(/Save/i)).toBeInTheDocument();
//       })
//   });
