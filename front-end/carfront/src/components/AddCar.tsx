import Dialog from '@mui/material/Dialog';
import  DialogActions  from '@mui/material/DialogActions';
import CarDialogContent from './CarDialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Car } from '../types';
import { useState } from 'react';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { addCar } from '../api/carapi';
import  Button  from '@mui/material/Button';

function AddCar() {

    const handleSave = () => {
        mutate(car);
        setCar({brand: '', model: '', color: '', registrationNumber: '', modelYear: 0, price: 0});
        handleClose();
    }

    const queryClient = useQueryClient();

    const { mutate } = useMutation(addCar, {
        onSuccess: () => {
            queryClient.invalidateQueries(['cars']);
        },
        onError: (err) => {
            console.error(err);
        },
    });

    const handleChange = (event : React.ChangeEvent<HTMLInputElement>) =>
        {
          setCar({...car, [event.target.name]:
              event.target.value});
        }

    const [open, setOpen] = useState(false);
    const [car, setCar] = useState<Car>({
        brand: '',
        model: '',
        color: '',
        registrationNumber: '',
        modelYear: 0,
        price: 0
    })

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };


    return(
        <>
        
        <Button onClick={handleClickOpen}>New Car</Button>
    <Dialog open={open} onClose={handleClose}>
      <DialogTitle>New car</DialogTitle> 
        <CarDialogContent car={car} handleChange={handleChange}/>
      <DialogActions>
         <Button onClick={handleClose}>Cancel</Button>
         <Button onClick={handleSave}>Save</Button>
      </DialogActions>
    </Dialog>   </>
    );
}

export default AddCar;