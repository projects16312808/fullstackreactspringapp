package com.packt.cardatabase;

import com.packt.cardatabase.web.CarController;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class CardatabaseApplicationTests {

	@Autowired
	CarController controller;

	@Test
	@DisplayName("First example test case")
	void contextLoads() {
		// Test to check if the context loads
		assertThat(controller).isNotNull();
	}
}
